#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);
unsigned long t_now;
char strikeline[16];
char timeline[16];
volatile int strikes;
int speed = 650;
int timer;
int modules = 1;

#define DEBUG
#ifdef DEBUG
  #define DBG(c) c;
#else
  #define DBG(c) ;
#endif
void ICACHE_RAM_ATTR interruptSolved(){
  DBG(Serial.println("solved"))
  modules--;
}

void ICACHE_RAM_ATTR interruptStrike(){
  DBG(Serial.println("Strike"))
  tone(13,200,250);
  strikes++;
}

void kill(){
  uint8_t v = 0;
  delay(100);
  while(1){
    v = v? 0: 255;
    lcd.setBacklight(v);
    tone(13,200,100);
    delay(1000-speed);
  };
}

void live(){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("You win");
  while(1){delay(10);};
}

void setup() {
  Serial.begin(9600);
  delay(2000);
  attachInterrupt(digitalPinToInterrupt(4),interruptSolved,FALLING);
  attachInterrupt(digitalPinToInterrupt(5),interruptStrike,FALLING);
  pinMode(4,INPUT_PULLUP);
  pinMode(5,INPUT_PULLUP);
  lcd.init();
  lcd.backlight();
  timer = 180;
}
void tick(){
  tone(13,2100,100);
  t_now = millis();
  while (millis()<t_now+350);
  tone(13,1800,100);
  lcd.setCursor(0,0);
  sprintf(strikeline,"      [%1s%1s]      ",((strikes - 1) >= 0)?"x":" ", ((strikes - 2)>=0)?"x":" ");
  lcd.print(strikeline);
  lcd.setCursor(0,1);
  sprintf(timeline,"    [%02d: %02d]    ",timer/60,timer%60);
  lcd.print(timeline);
};

void loop() {
  while (timer>=0){
    //logic();
    tick();
    delay(speed);
    timer--;
    if(strikes>=2){
      tick();
      break;
    };
    if(modules <= 0){
      live();
    }
  };
  kill();
} 