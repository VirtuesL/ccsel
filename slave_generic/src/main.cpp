#include <Arduino.h>

int pins[] =  { A3/*1*/, A6 /*2*/, A2/*3*/,  A7/*4*/, A0/*5*/, A8/*6*/ };

int w;
bool even;
int fail;

void setup() {
  Serial.begin(9600);
  while(!Serial){}; 
  while (!Serial.available()){}
  even = (Serial.read() == 'e')? true:false;
  if(even){
    fail=pins[3];
  }else{
    fail = pins[0];
  }
};

void loop() {
  Serial.print("we're "); Serial.println(even);
  for (auto p: pins){
    float s = (float)analogRead(p)/1024;
    Serial.println(s);
  }
  while(1);
  
}
